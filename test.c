#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "Client_Serveur_V2/Serveur/database.h"

bool callback_mess(Message * m, void * context){
	printf("context:%s\n", (char *) context);
	printf("message:\n");
	printf("	text:%s\n", m->text);
	printf("	sender:%s\n", m->sender);
	return true;
}

bool callback_room(Room * r, void * context){
	printf("context:%s\n", (char *) context);
	printf("room:\n");
	printf("	name:%s\n", r->name);
	printf("	author:%s\n", r->author);
	return true;
}

int main(){
	srand((unsigned) time(NULL));
	printf("toB64(\"mariosalty\"):%s\n", toB64((unsigned char *)"mariosalty",10));
	printf("hashIt(\"mario\",\"salty\"):%s\n", hashIt("mario","salty"));
	char message[8]="";
	strcpy(message,random_string(7));
	printf("random_string(7):%s\n", message);

	printf("connectDB(\"db.sqlite\"):%i\n",connectDB("db.sqlite"));

	printf("userExist (\"cobail\"):%s\n",userExist ("cobail")?"true":"false");
	printf("roomExist (\"testit\"):%s\n",roomExist ("testit")?"true":"false");

	printf("createUser(\"cobail\", \"1234\"):%s\n",createUser("cobail", "1234")?"true":"false");
	printf("createUser(\"cobail\", \"12345\"):%s\n",createUser("cobail", "12345")?"true":"false");

	printf("userExist (\"cobail\"):%s\n",userExist ("cobail")?"true":"false");
	
	printf("createRoom(\"testit\", \"not_found\"):%s\n",createRoom("testit", "not_found")?"true":"false");
	printf("createRoom(\"testit\", \"cobail\"):%s\n",createRoom("testit", "cobail")?"true":"false");
	printf("createRoom(\"testit\", \"cobail\"):%s\n",createRoom("testit", "cobail")?"true":"false");

	printf("roomExist (\"testit\"):%s\n",roomExist ("testit")?"true":"false");

	char salt[17];
	printf("getUserSalt(\"not_found\"):%s\n",getUserSalt("not_found",salt)?"true":"false");
	printf("salt:%s\n",salt); 
	printf("getUserSalt(\"cobail\"):%s\n",getUserSalt("cobail",salt)?"true":"false");
	printf("salt:%s\n",salt); 

	printf("checkUserPassword(\"not_found\",\"1234\"):%s\n",checkUserPassword("not_found","1234")?"true":"false");
	printf("checkUserPassword(\"cobail\",\"false\"):%s\n",checkUserPassword("cobail","false")?"true":"false");
	printf("checkUserPassword(\"cobail\",\"12345\"):%s\n",checkUserPassword("cobail","12345")?"true":"false");
	printf("checkUserPassword(\"cobail\",\"1234\"):%s\n",checkUserPassword("cobail","1234")?"true":"false");
	
	printf("getRoomMessageCount(\"not_found\"):%i\n",getRoomMessageCount("not_found"));
	printf("getRoomMessageCount(\"testit\"):%i\n",getRoomMessageCount("testit"));

	printf("userIsRoomOwner(\"testit\",\"cobail\"):%s\n",userIsRoomOwner("testit","cobail")?"true":"false");
	printf(
			"saveMessage(\"cobail\", \"testit\", \"%s\"):%s\n",
			message,
			saveMessage("cobail", "testit", message)?"true":"false"
		);

	printf(
			"getMessages (\"testit\",&callback,\"bonjour\",-1):%s\n",
			getMessages("testit",callback_mess,"bonjour",-1)?"true":"false"
		);

	printf(
			"getRooms (\"\",&callback_room,\"bonjour\",-1):%s\n",
			getRooms("",callback_room,"bonjour",-1)?"true":"false"
		);
	printf("deleteUser(\"cobail\"):%s\n",deleteUser("cobail")?"true":"false");
	printf("deleteRoom(\"testit\"):%s\n",deleteRoom("testit")?"true":"false");
	
	printf("userExist (\"cobail\"):%s\n",userExist ("cobail")?"true":"false");
	printf("roomExist (\"testit\"):%s\n",roomExist ("testit")?"true":"false");
	
	return 0;
}
