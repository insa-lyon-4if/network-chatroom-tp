main: dataFrame.o database.o
	gcc -g bin/dataFrame.o "Client_Serveur_V2/Client/client2.c" -o bin/client
	gcc -g bin/dataFrame.o bin/database.o "Client_Serveur_V2/Serveur/server2.c" -o bin/server -lsqlite3 -lcrypto

test: dataFrame.o database.o
	gcc -g bin/dataFrame.o bin/database.o test.c -lsqlite3 -lcrypto -o bin/test

database.o:
	gcc -g -c Client_Serveur_V2/Serveur/database.c -o bin/database.o

dataFrame.o:
	gcc -g -c Client_Serveur_V2/dataFrame.c -o bin/dataFrame.o

clear:
	rm bin/*
