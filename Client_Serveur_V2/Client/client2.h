#ifndef CLIENT_H
#define CLIENT_H

#ifdef WIN32

#include <winsock2.h>

#elif defined (linux)

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else

#error not defined for this platform

#endif

#define CRLF     "\r\n"
#define PORT     1977


#include "../defined.h"
#include "../dataFrame.h"

static void init(void);
static void end(void);
static void app(const char *address, const char *name, const char *password, char function);
static int init_connection(const char *address);
static void end_connection(int sock);
static int read_server(SOCKET sock, void** data_adr);
static void write_server(SOCKET sock, void *data, size_t data_size);
static void send_login(SOCKET sock, const char * login_st, const char * password, char function);
static void send_message(SOCKET sock, const char * login, const char *buffer);
void print_help(void);

#endif /* guard */
