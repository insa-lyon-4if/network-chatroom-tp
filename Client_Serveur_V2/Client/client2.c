#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "client2.h"

static void init(void)
{
#ifdef WIN32
	WSADATA wsa;
	int err = WSAStartup(MAKEWORD(2, 2), &wsa);
	if(err < 0)
	{
		puts("WSAStartup failed !");
		exit(EXIT_FAILURE);
	}
#endif
}

static void end(void)
{
#ifdef WIN32
	WSACleanup();
#endif
}

static void send_login(SOCKET sock, const char * login_st, const char * password, char function)
{

	Frame frame;
	frame.id = 0;
	frame.type = 'l';
	Login login;
	strcpy(login.login, login_st);
	strcpy(login.password, password);
	switch (function)
	{
		case 'l':
			login.isNew = 0;
		break;
		case 'r':
			login.isNew = 1;
		break;
		default:
		break;
	}

	frame.data.l=login;
	
	void * data;
	size_t data_size=save_distant(&data, &frame);
	
	write_server(sock, data, data_size);
	free(data);

}

void print_help()
{
	printf("[\033[31mHELP\033[0m]\n"
			"\033[1m/+room_name\033[0m : create room\n"
			"\033[1m/-room_name\033[0m : delete room\n"
			"\033[1m/>room_name\033[0m : join room\n"
			"\033[1m/<\033[0m : leave room\n"
			"\033[1m/*\033[0m : list all rooms\n"
			"\033[1m/?\033[0m : show help\033[1m\n"
			);
}

static void send_message(SOCKET sock, const char * login, const char *buffer)
{
	if (strlen(buffer) > 0)
	{
		if (buffer[0] == '/')
		{
			if (strlen(buffer) > 1)
			{
				Frame frame;
				frame.id = 0;
				frame.type = 'c';
				Command command;
				command.senderLen=BUF_SIZE;
				command.sender=malloc(command.senderLen*sizeof(char));
				strncpy(command.sender, login, command.senderLen);
				command.command = buffer[1];
				
				switch(buffer[1])
				{
					case '+': {
						if (strlen(buffer) > 2)
						{
							//fprintf(stderr, "buffer: %s\n", buffer+2);
							strcpy(command.roomName, buffer+2);
							frame.data.c = command;
							void * data;
							size_t data_size=save_distant(&data, &frame);
							write_server(sock, data, data_size);
							free(data);
						}
						else
						{
							print_help();
						}
					} break;
					case '-': {
						if (strlen(buffer) > 2)
						{
							//fprintf(stderr, "buffer: %s\n", buffer+2);
							strcpy(command.roomName, buffer+2);
							frame.data.c = command;
							void * data;
							size_t data_size=save_distant(&data, &frame);
							write_server(sock, data, data_size);
							free(data);
						}
						else
						{
							print_help();
						}
					} break;
					case '>': {
						if (strlen(buffer) > 2)
						{
							//fprintf(stderr, "buffer: %s\n", buffer+2);
							strcpy(command.roomName, buffer+2);
							frame.data.c = command;
							void * data;
							size_t data_size=save_distant(&data, &frame);
							write_server(sock, data, data_size);
							free(data);
						}
						else
						{
							print_help();
						}
					} break;
					case '<': {
						//fprintf(stderr, "buffer: %s\n", buffer+1);
						frame.data.c = command;
						void * data;
						size_t data_size=save_distant(&data, &frame);
						write_server(sock, data, data_size);
						free(data);
					} break;
					case '*': {
						//fprintf(stderr, "buffer: %s\n", buffer+1);
						frame.data.c = command;
						void * data;
						size_t data_size=save_distant(&data, &frame);
						write_server(sock, data, data_size);
						free(data);
					} break;
					case '?':
						print_help();
					break;
					default:
						print_help();
					break;
					
				}
				free(command.sender);
			}
			else
			{
				print_help();
			}
		}

		else
		{
			Frame frame;
			frame.id = 0;
			frame.type = 'm';
			Message message;
			message.senderLen=BUF_SIZE;
			message.sender=malloc(message.senderLen*sizeof(char));
			strncpy(message.sender, login, message.senderLen);
			message.textLen=strlen(buffer)+1;
			message.text=malloc(message.textLen*sizeof(char));
			strncpy(message.text, buffer, message.textLen);
			frame.data.m = message;

			void * data;
			size_t data_size=save_distant(&data, &frame);
			write_server(sock, data, data_size);
			free(data);
			free(message.sender);
			free(message.text);
		}
	}

}
static void app(const char *address, const char *name, const char *password, char function)
{
	SOCKET sock = init_connection(address);
	char buffer[BUF_SIZE];

	Frame frame;

	fd_set rdfs;

	/* send our name */
	send_login(sock, name, password, function);

	while(1)
	{
		FD_ZERO(&rdfs);

		/* add STDIN_FILENO */
		FD_SET(STDIN_FILENO, &rdfs);

		/* add the socket */
		FD_SET(sock, &rdfs);

		if(select(sock + 1, &rdfs, NULL, NULL, NULL) == -1)
		{
			perror("select()");
			exit(errno);
		}

		/* something from standard input : i.e keyboard */
		if(FD_ISSET(STDIN_FILENO, &rdfs))
		{
			fgets(buffer, BUF_SIZE - 1, stdin);
			{
				char *p = NULL;
				p = strstr(buffer, "\n");
				if(p != NULL)
				{
					*p = 0;
				}
				else
				{
					/* fclean */
					buffer[BUF_SIZE - 1] = 0;
				}
			}
			send_message(sock, name, buffer);
		}
		else if(FD_ISSET(sock, &rdfs))
		{
			printf("\033[0m");

			void * data;
			int n = read_server(sock, &data);
			/* server down */
			if(n == 0)
			{
				printf("\033[31mServer disconnected !\033[0m\n");
				break;
			}
			load_distant(data,&frame);
			//puts(buffer);
			switch(frame.type)
			{
				case 'm': {
					Message m = frame.data.m;
					printf("[%s] %s\033[1m\n",
							m.sender,
							m.text
							);
				} break;
				case 'l':
				break;
				case 'c':
				break;
				case 'i': {
					switch (frame.data.i.scope)
					{
						case 1:
							printf("[\033[35mSERVER-global\033[0m] %s\033[1m\n",
									frame.data.i.text
									);
						break;
						case 2:
							printf("[\033[35mSERVER-room\033[0m] %s\033[1m\n",
									frame.data.i.text
									);
						break;
						case 3:
							printf("[\033[35mSERVER-user\033[0m] %s\033[1m\n",
									frame.data.i.text
									);
						break;
						case 4: {
							// message from server that forces client to disconnect
							printf("[\033[35mSERVER-user\033[0m] %s\033[1m\n",
									frame.data.i.text
									);
							free(data);
							destroy(frame);
							end_connection(sock);
							return;
						} break;
					}
				} break;
				case 'e':
				break;
			}
  			
			free(data);
			destroy(frame);
		}
	}

	end_connection(sock);
}

static int init_connection(const char *address)
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	SOCKADDR_IN sin = { 0 };
	struct hostent *hostinfo;

	if(sock == INVALID_SOCKET)
	{
		perror("socket()");
		exit(errno);
	}

	hostinfo = gethostbyname(address);
	if (hostinfo == NULL)
	{
		fprintf(stderr, "Unknown host %s.\n", address);
		exit(EXIT_FAILURE);
	}

	sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
	sin.sin_port = htons(PORT);
	sin.sin_family = AF_INET;

	if(connect(sock, (SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
	{
		perror("connect()");
		exit(errno);
	}

	return sock;
}

static void end_connection(int sock)
{
	closesocket(sock);
}

static int read_server(SOCKET sock, void **data_adr)
{
	int n = 0;
	Frame frame;

	if((n = recv(sock, &frame, sizeof(Frame), 0)) < 0)
	{
		perror("recv()");
		exit(errno);
	}
	if(n==0)
	{
		return n;
	}
	size_t size=real_frame_size(&frame);

	void * data=malloc(size);
	memcpy(data, &frame, sizeof(Frame));

	if((n = recv(sock, data+sizeof(frame), size-sizeof(Frame), 0)) < 0)
	{
		perror("recv()");
		exit(errno);
	}

	(*data_adr)=data;

	return n;
}

static void write_server(SOCKET sock, void *data, size_t data_size)
{
	if(send(sock, data, data_size, 0) < 0)
	{
		perror("send()");
		exit(errno);
	}
	
}

int main(int argc, char **argv)
{
	if(argc < 5 || ((char)*argv[4] != 'l' && (char)*argv[4] != 'r'))
	{
		printf("Usage : %s [address] [pseudo] [password] [(l)ogin/(r)egister]\n", argv[0]);
		return EXIT_FAILURE;
	}
	printf("\033[1m");

	init();

	app(argv[1], argv[2], argv[3], *argv[4]);

	end();

	return EXIT_SUCCESS;
}
