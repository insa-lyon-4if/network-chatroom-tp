#ifndef FRAME_H
#define FRAME_H

#include "defined.h"

typedef struct
{
	int senderLen;
	char* sender;
	int textLen;
	char* text;
}Message; //m

typedef struct
{
	char login[BUF_SIZE];
	char password[BUF_SIZE];
	char isNew;
}Login; //l

typedef struct
{
	int senderLen;
	char* sender;
	char command;
	char roomName[BUF_SIZE];
}Command; //c

typedef struct
{
	int textLen;
	char* text;
	char scope;
}Info; //i

typedef struct
{
	int code;
	char message[BUF_SIZE];
}Error; //e

typedef union
{
	Login l;
	Message m;
	Command c;
	Info i;
	Error e;
}FrameData;

typedef struct
{
	int id;
	char type;
	FrameData data;
}Frame;

#include <stddef.h>

void load_distant(void* data, Frame* frame);
size_t save_distant(void** data_adr, Frame* frame);
size_t real_frame_size(Frame* frame);
void destroy(Frame frame);

#endif /* guard */