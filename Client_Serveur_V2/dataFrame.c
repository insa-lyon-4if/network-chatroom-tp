#include "dataFrame.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

size_t real_frame_size(Frame* frame)
{
	size_t size=sizeof(Frame);

	switch(frame->type)
	{
		case 'm':
			size+=frame->data.m.senderLen*sizeof(char);
			size+=frame->data.m.textLen*sizeof(char);
			break;
		case 'l':
			break;
		case 'c':
			size+=frame->data.c.senderLen*sizeof(char);
			break;
		case 'i':
			size+=frame->data.i.textLen*sizeof(char);
			break;
		case 'e':
			break;
	}
	return size;
}
void load_distant(void* data, Frame* frame)
{
	memcpy(frame,data,sizeof(Frame));
	void * data_pt=data;
	data_pt+=sizeof(Frame);
	int len;
	switch(frame->type)
	{
		case 'm':
			len=frame->data.m.senderLen;
			frame->data.m.sender=malloc(len*sizeof(char));
			memcpy(frame->data.m.sender, data_pt, len*sizeof(char));
			data_pt+=len*sizeof(char);
			len=frame->data.m.textLen;
			frame->data.m.text=malloc(len*sizeof(char));
			memcpy(frame->data.m.text, data_pt, len*sizeof(char));
			break;
		case 'l':
			break;
		case 'c':
			len=frame->data.c.senderLen;
			frame->data.c.sender=malloc(len*sizeof(char));
			memcpy(frame->data.c.sender, data_pt, len*sizeof(char));
			break;
		case 'i':
			len=frame->data.i.textLen;
			frame->data.i.text=malloc(len*sizeof(char));
			memcpy(frame->data.i.text, data_pt, len*sizeof(char));
			break;
		case 'e':
			break;
	}
}
size_t save_distant(void ** data_addr, Frame* frame)
{
	size_t size=real_frame_size(frame);
	void * data=malloc(size);
	memcpy(data,frame,sizeof(Frame));
	void * data_pt=data;
	data_pt+=sizeof(Frame);
	int len;
	switch(frame->type)
	{
		case 'm':
			len=frame->data.m.senderLen;
			memcpy(data_pt, frame->data.m.sender, len*sizeof(char));
			data_pt+=len*sizeof(char);
			len=frame->data.m.textLen;
			memcpy(data_pt, frame->data.m.text, len*sizeof(char));
			break;
		case 'l':
			break;
		case 'c':
			len=frame->data.c.senderLen;
			memcpy(data_pt, frame->data.c.sender, len*sizeof(char));
			break;
		case 'i':
			len=frame->data.i.textLen;
			memcpy(data_pt, frame->data.i.text, len*sizeof(char));
			break;
		case 'e':
			break;
	}
	(*data_addr)=data;

	return size;
}

void destroy(Frame frame)
{
	switch(frame.type)
	{
		case 'm':
			free(frame.data.m.sender);
			free(frame.data.m.text);
			break;
		case 'l':
			break;
		case 'c':
			free(frame.data.c.sender);
			break;
		case 'i':
			free(frame.data.i.text);
			break;
		case 'e':
			break;
	}
}