#ifndef DEFINED_H
#define DEFINED_H

#define CRLF          "\r\n"
#define PORT          1977
#define MAX_CLIENTS   100

#define BUF_SIZE      1024

#endif
