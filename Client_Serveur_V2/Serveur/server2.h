#ifndef SERVER_H
#define SERVER_H

#ifdef WIN32

#include <winsock2.h>

#elif defined (linux)

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else

#error not defined for this platform

#endif

#include "../defined.h"

#include "../dataFrame.h"
#include "database.h"
#include "client2.h"


static void init(void);
static void end(void);
static void app(void);
static int init_connection(void);
static void end_connection(int sock);
static int read_client(SOCKET sock, void **buffer);
static void write_client(SOCKET sock, void * data, size_t data_size);
static void send_message_to_all_clients(Client *clients, Client client, int nClients, const char *buffer, char from_server);
static void send_message_to_client_room(Client *clients, Client client, int nClients, const char *buffer, char from_server);
static void send_message_to_client(Client reciever, Client sender, const char *buffer, char from_server);
static void remove_client(Client *clients, int to_remove, int *nClients);
static void clear_clients(Client *clients, int nClients);
void create_room(const char *roomName, Client *client);
void delete_room(const char *roomName, Client *clients, int nClients, Client *client);
void join_room(const char *roomName, Client *clients, int nClients, Client *client);
void leave_room(Client *clients, int nClients, Client *client);
void list_rooms(Client client);
void log_client(Client *clients, int *nClients, Frame frame, int sock);
void register_client(Client *clients, int *nClients, Frame frame, int sock);
void send_history(Client client);

#endif /* guard */
