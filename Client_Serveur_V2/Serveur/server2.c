#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "server2.h"
#include "client2.h"

static void init(void)
{
	if(!connectDB("db.sqlite"))exit(EXIT_FAILURE);
	srand((unsigned) time(NULL));
#ifdef WIN32
	WSADATA wsa;
	int err = WSAStartup(MAKEWORD(2, 2), &wsa);
	if(err < 0)
	{
		puts("WSAStartup failed !");
		exit(EXIT_FAILURE);
	}
#endif
}

static void end(void)
{
	sqlite3_close(db);
#ifdef WIN32
	WSACleanup();
#endif
}

static void app(void)
{
	SOCKET sock = init_connection();
	void * data;
	/* the index for the array */
	int nClients = 0;
	//int nRooms = 0;
	int max = sock;
	/* an array for all clients */
	Client clients[MAX_CLIENTS];
	//Room rooms[MAX_ROOMS*MAX_CLIENTS];

	fd_set rdfs;

	while(1)
	{
		int i = 0;
		FD_ZERO(&rdfs);

		/* add STDIN_FILENO */
		FD_SET(STDIN_FILENO, &rdfs);

		/* add the connection socket */
		FD_SET(sock, &rdfs);

		/* add socket of each client */
		for(i = 0; i < nClients; i++)
		{
			FD_SET(clients[i].sock, &rdfs);
		}

		if(select(max + 1, &rdfs, NULL, NULL, NULL) == -1)
		{
			perror("select()");
			exit(errno);
		}

		/* something from standard input : i.e keyboard */
		if(FD_ISSET(STDIN_FILENO, &rdfs))
		{
			/* stop process when type on keyboard */
			break;
		}
		else if(FD_ISSET(sock, &rdfs))
		{
			/* new client */
			SOCKADDR_IN csin = { 0 };
			socklen_t sinsize = sizeof csin;
			int csock = accept(sock, (SOCKADDR *)&csin, &sinsize);
			if(csock == SOCKET_ERROR)
			{
				perror("accept()");
				continue;
			}
			int n = read_client(csock, &data);
			/* after connecting the client sends its name */
			if(n == 0)
			{
				/* disconnected */
				continue;
			}
			
			Frame frame;
			load_distant(data,&frame);
			/* what is the new maximum fd ? */
			max = csock > max ? csock : max;

			FD_SET(csock, &rdfs);

			if (frame.data.l.isNew)
			{
				register_client(clients, &nClients, frame, csock);
			}
			else
			{
				log_client(clients, &nClients, frame, csock);
			}


			free(data);
			destroy(frame);

			
		}
		else
		{
			int i = 0;
			for(i = 0; i < nClients; i++)
			{
				/* a client is talking */
				if(FD_ISSET(clients[i].sock, &rdfs))
				{
					Client client = clients[i];
					int c = read_client(clients[i].sock, &data);
					/* client disconnected */
					if(c == 0)
					{
						char buffer[BUF_SIZE];
						closesocket(clients[i].sock);
						remove_client(clients, i, &nClients);
						strncpy(buffer, client.name, BUF_SIZE - 1);
						strncat(buffer, " disconnected!", BUF_SIZE - strlen(buffer) - 1);
						send_message_to_all_clients(clients, client, nClients, buffer, 1);
					}
					else
					{
						Frame frame;
				 		load_distant(data,&frame);
						switch (frame.type)
						{
							case 'm':
								send_message_to_client_room(clients, client, nClients, frame.data.m.text, 0);
							break;
							case 'c': {
								Command command = frame.data.c;
								switch (command.command)
								{
									case '+': {
										create_room(command.roomName, clients+i);
									} break;
									case '-': {
										delete_room(command.roomName, clients, nClients, clients+i);
									} break;
									case '>': {
										join_room(command.roomName, clients, nClients, clients+i);
									} break;
									case '<': {
										leave_room(clients, nClients, clients+i);
									} break;
									case '*': {
										list_rooms(client);
									} break;
									default:
									break;
								}
								//fprintf(stderr, "command: %c\n", frame.data.c.command);
							} break;
						}
						free(data);
						destroy(frame);
						
					}
					break;
				}
			}
		}
	}

	clear_clients(clients, nClients);
	end_connection(sock);
}

static void clear_clients(Client *clients, int nClients)
{
	int i = 0;
	for(i = 0; i < nClients; i++)
	{
		closesocket(clients[i].sock);
	}
}
static void remove_client(Client *clients, int to_remove, int *nClients)
{
	/* we remove the client in the array */
	memmove(clients + to_remove, clients + to_remove + 1, (*nClients - to_remove - 1) * sizeof(Client));
	/* number client - 1 */
	(*nClients)--;
}

static void send_message_to_all_clients(Client *clients, Client sender, int nClients, const char *buffer, char from_server)
{
	int i = 0;
	for(i = 0; i < nClients; i++)
	{
		/* we don't send message to the sender */
		if(sender.sock != clients[i].sock)
		{
			send_message_to_client(clients[i], sender, buffer, from_server);
		}
	}
}

static void send_message_to_client_room(Client *clients, Client sender, int nClients, const char *buffer, char from_server)
{	
	if (!from_server)
	{
		// message from client to clients
		saveMessage(sender.name, sender.roomName, buffer);
	}
	int i = 0;
	for(i = 0; i < nClients; i++)
	{
		/* we don't send message to the sender */
		if(sender.sock != clients[i].sock && !strcmp(sender.roomName,clients[i].roomName) )
		{
			send_message_to_client(clients[i], sender, buffer, from_server);
		}
	}
}

static void send_message_to_client(Client reciever, Client sender, const char *buffer, char from_server)
{
	Frame frame;
	frame.id = 0;
	if (from_server)
	{
		frame.type = 'i';
		Info info;
		info.textLen=strlen(buffer)+1;
		info.text=malloc(info.textLen*sizeof(char));
		strncpy(info.text, buffer, info.textLen);
		info.scope = from_server;
		frame.data.i = info;
	}
	else
	{
		frame.type = 'm';
		Message message;
		message.senderLen=BUF_SIZE;
		message.sender=malloc(message.senderLen*sizeof(char));
		strncpy(message.sender, sender.name, message.senderLen);
		message.textLen=strlen(buffer)+1;
		message.text=malloc(message.textLen*sizeof(char));
		strncpy(message.text, buffer, message.textLen);
		frame.data.m = message;
	}
	

	void * data;
	size_t data_size=save_distant(&data, &frame);
	write_client(reciever.sock, data, data_size);
	free(data);
	destroy(frame);
}

static int init_connection(void)
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	SOCKADDR_IN sin = { 0 };

	if(sock == INVALID_SOCKET)
	{
		perror("socket()");
		exit(errno);
	}
	
	int opt = 1;
	if(setsockopt(sock,SOL_SOCKET,SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
	{
		perror("setsockopt()");
		exit(EXIT_FAILURE);
	}

	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(PORT);
	sin.sin_family = AF_INET;

	if(bind(sock,(SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR)
	{
		perror("bind()");
		exit(errno);
	}

	if(listen(sock, MAX_CLIENTS) == SOCKET_ERROR)
	{
		perror("listen()");
		exit(errno);
	}

	return sock;
}

static void end_connection(int sock)
{
	closesocket(sock);
}

static int read_client(SOCKET sock, void **data_adr)
{
	int n = 0;
	Frame frame;

	if((n = recv(sock, &frame, sizeof(Frame), 0)) < 0)
	{
		perror("recv()");
		exit(errno);
	}
	if(n==0)
	{
		return n;
	}

	size_t size=real_frame_size(&frame);

	void * data=malloc(size);
	memcpy(data, &frame, sizeof(Frame));


	if( sizeof(Frame)!=size && (n = recv(sock, data+sizeof(Frame), size-sizeof(Frame), 0)) < 0)
	{
		perror("recv()");
		exit(errno);
	}

	(*data_adr)=data;

	return n;
}

static void write_client(SOCKET sock, void* data, size_t data_size)
{
	if(send(sock, data, data_size, 0) < 0)
	{
		perror("send()");
		exit(errno);
	}
}

void create_room(const char *roomName, Client* client)
{
	if(!createRoom(roomName,client->name)){
		char buffer[BUF_SIZE] = "The room name <";
		strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, "> is not available.", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);
	}else{
		char buffer[BUF_SIZE] = "The room <";
		strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, "> has been successfully created!", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);
	}
}

void delete_room(const char *roomName, Client *clients, int nClients, Client *client)
{

	if (!roomExist(roomName))
	{
		//fprintf(stderr, "NO ROOM: %s\n", roomName);
		char buffer[BUF_SIZE] = "The room <";
		strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, "> does not exist...", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);
	}
	else
	{

		if (userIsRoomOwner(roomName, client->name))
		{
			if (deleteRoom(roomName))
			{
				char buffer[BUF_SIZE] = "You have successfully deleted the room <";
				strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
				strncat(buffer, ">.", BUF_SIZE - strlen(buffer) - 1);
				send_message_to_client(*client, *client, buffer, 3);
				strcpy(buffer,"The owner of the room <");
				strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
				strncat(buffer, "> just deleted it, so you have been moved to the lobby.", BUF_SIZE - strlen(buffer) - 1);
				strcpy(client->roomName,"");
				for (int i=0; i<nClients; ++i)
				{
					if (!strcmp(clients[i].roomName, roomName))
					{
						strcpy(clients[i].roomName,"");
						send_message_to_client(clients[i], *client, buffer, 2);
					}
				}

			}
		}
		else
		{
			//fprintf(stderr, "NOT YOUR ROOM: %s\n", roomName);
			char buffer[BUF_SIZE] = "You cannot delete the room <";
			strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
			strncat(buffer, "> as you are not its owner.", BUF_SIZE - strlen(buffer) - 1);
			send_message_to_client(*client, *client, buffer, 3);
		}

	}

}

void join_room(const char *roomName, Client *clients, int nClients, Client *client)
{
	if (!roomExist(roomName))
	{
		char buffer[BUF_SIZE] = "The room <";
		strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, "> does not exist...", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);
	}
	else if (!strcmp(roomName, client->roomName))
	{
		char buffer[BUF_SIZE] = "You are already in the room <";
		strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, ">.", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);
	}
	else
	{
		//client->room = (Room*) room;
		strncpy(client->roomName,roomName, BUF_SIZE);
		char buffer[BUF_SIZE] = "You have successfully joined the room <";
		strncat(buffer, roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, ">!", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);

		strncpy(buffer,"<",BUF_SIZE);
		strncat(buffer, client->name, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, "> just slid into the room.", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client_room(clients, *client, nClients, buffer, 2);

		send_history(*client);
	}
}

void leave_room(Client *clients, int nClients, Client *client)
{
	char buffer[BUF_SIZE];
	if (strlen(client->roomName) == 0)
	{
		strncpy(buffer, "You are already in the lobby.", BUF_SIZE);
		send_message_to_client(*client, *client, buffer, 3);
	}
	else
	{
		strncpy(buffer, "You have successfully left the room <", BUF_SIZE);
		strncat(buffer, client->roomName, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, ">.", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client(*client, *client, buffer, 3);

		strncpy(buffer,"<",BUF_SIZE);
		strncat(buffer, client->name, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, "> left the room.", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_client_room(clients, *client, nClients, buffer, 2);

		strcpy(client->roomName,"");
	}
}

bool list_callback(Room * r, void * c)
{
	if (strlen(r->name) == 0)
	{
		return true;
	}
	
	char buffer[1048]="";
	Client * client = (Client *) c;
	strcat(buffer, " <");
	if(strcmp(r->author,client->name) == 0){
		strcat(buffer, "\033[1m");
	}
	strcat(buffer, r->name);
	strcat(buffer, "\033[0m>");
	send_message_to_client(*client, *client, buffer, 3);
	return true;
}

void list_rooms(Client client)
{
	send_message_to_client(client, client, "List of (\033[1myour\033[0m) rooms:", 3);
	getRooms("",list_callback,&client,-1);
	return;
}

bool history_callback(Message * m, void * c)
{
	Client * client = (Client *) c;
	Client sender = {};
	strcpy(sender.name, m->sender);
	strcpy(sender.roomName, client->roomName);
	send_message_to_client(*client, sender, m->text, 0);
	return true;
}

void send_history(Client client)
{
	getMessages(client.roomName,history_callback,&client,-1);
	return;
}

void log_client(Client *clients, int *nClients, Frame frame, int csock)
{
	Client c = { csock };
	strncpy(c.name, frame.data.l.login, BUF_SIZE);
	strncpy(c.password, frame.data.l.password, BUF_SIZE);
	c.roomName[0] = '\0';

	int i = 0;
	for(i = 0; i < *nClients; ++i)
	{
		if (!strcmp(c.name, clients[i].name))
		{
			char buffer[BUF_SIZE] = "You are already connected!";
			send_message_to_client(c, c, buffer, 4);
			closesocket(c.sock);
			return;
		}
	}
	
	if (!checkUserPassword(c.name,c.password))
	{
		char buffer[BUF_SIZE] = "Wrong login or password.";
		send_message_to_client(c, c, buffer, 4);
		closesocket(c.sock);
	}
	else if (*nClients == MAX_CLIENTS)
	{
		send_message_to_client(c, c, "No more slot to join the server.", 4);
		closesocket(c.sock);
	}
	else
	{
		clients[*nClients] = c;
		++(*nClients);
		send_message_to_client(clients[i], clients[i], "Welcome back!", 3);
		char buffer[BUF_SIZE] = "";
		strncat(buffer, c.name, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, " logged in!", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_all_clients(clients, c, *nClients, buffer, 1);

		send_history(c);
	}
				

}

void register_client(Client *clients, int *nClients, Frame frame, int csock)
{
	Client c = { csock };
	strncpy(c.name, frame.data.l.login, BUF_SIZE);
	strncpy(c.password, frame.data.l.password, BUF_SIZE);
	c.roomName[0] = '\0';


	if (!createUser(c.name,c.password))
	{
		char buffer[BUF_SIZE] = "This login is not available.";
		send_message_to_client(c, c, buffer, 4);
		closesocket(c.sock);
	}
	else
	{
		clients[*nClients] = c;
		++(*nClients);
		char buffer[BUF_SIZE] = "";
		strncat(buffer, c.name, BUF_SIZE - strlen(buffer) - 1);
		strncat(buffer, " logged in!", BUF_SIZE - strlen(buffer) - 1);
		send_message_to_all_clients(clients, c, *nClients, buffer, 1);

		send_message_to_client(c, c, "Successful account creation.", 3);
	}
}

int main(int argc, char **argv)
{
	init();

	app();

	end();

	return EXIT_SUCCESS;
}
