#ifndef CLIENT_H
#define CLIENT_H

#include <sys/socket.h>
typedef int SOCKET;

#include "../defined.h"

typedef struct
{
	char name[BUF_SIZE];
	char author[BUF_SIZE];
}Room;

typedef struct
{
	SOCKET sock;
	char name[BUF_SIZE];
	char password[BUF_SIZE];
	char roomName[BUF_SIZE];
}Client;




#endif /* guard */
