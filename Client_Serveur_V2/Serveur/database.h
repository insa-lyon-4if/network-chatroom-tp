#ifndef DATABASE_H
#define DATABASE_H

#include <sqlite3.h>
#include <stdbool.h>

#include "client2.h"
#include "../dataFrame.h"
#include "../defined.h"


extern sqlite3 *db;

int connectDB(const char * name);

char * toB64(const unsigned char* data, int len);

char * random_string(size_t size);

const char * hashIt(const char * pwd, const char * salt);

bool saveMessage (
		char author[BUF_SIZE],
		char room[BUF_SIZE],
		const char * message
	);

bool getMessages (
		char room[BUF_SIZE],
		bool (*callback)(Message *, void *),
		void * extra_parameters,
		int maxCount
	);



bool getUserSalt (
		const char * login,
		char salt[17]
	);


bool checkUserPassword (
		const char * login,
		const char * passwd
	);

bool createUser (
		const char * login,
		const char * passwd
	);

bool deleteUser (
		const char * login
	);

bool userExist (
		const char * login
	);

int getRoomMessageCount (
		const char * room_name
	);

bool getRooms (
		const char * author,
		bool (*callback)(Room *, void *),
		void * extra_parameters,
		int maxCount
	);

bool createRoom (
		const char * room_name,
		const char * author
	);

bool deleteRoom (
		const char * room_name
	);

bool roomExist (
		const char * room_name
	);

bool userIsRoomOwner (
		const char * room_name,
		const char * author
	);
#endif

/*typedef struct
{
	int senderLen;
	char* sender;
	int textLen;
	char* text;
}Message; //m

*/
