#include "database.h"
#include <openssl/sha.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

sqlite3 *db;

int connectDB(const char * name){
	int rc = sqlite3_open(name, &db);
	if(rc){
		fprintf(stderr, "fail to open db [%s]\n", sqlite3_errmsg(db));
		return 0;
	}
	return 1;
}

bool saveMessage (
		char author[1024],
		char room[1024],
		const char * message
	){
	if(!roomExist(room)){
		return false;
	}
	int count=getRoomMessageCount(room);

	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"insert into messages "
			"(idx, author, room, message) "
			"values (?,?,?,?);",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare save message [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_int(prepared_sql, 1, count)!=SQLITE_OK){
		fprintf(stderr, "fail to bind idx\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 2, author, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind author\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 3, room, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind rooom\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 4, message, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind message\n");
		free(prepared_sql);
		return false;
	}
	
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}

bool getMessages (
		char room[1024],
		bool (*callback)(Message *, void *),
		void * extra_parameters,
		int maxCount
	){
	if(!roomExist(room)){
		return false;
	}
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select author, message "
			"from messages "
			"where room = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare get message [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, room, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind room\n");
		free(prepared_sql);
		return false;
	}


	int i=-maxCount;
	while (true){
		if(i==0){
			break;
		}
		i++;
		rc=sqlite3_step(prepared_sql);
		if(rc!=SQLITE_ROW){
			break;
		}
		Message m;
		const char * buffer;
		buffer=sqlite3_column_text(prepared_sql, 0);
		m.senderLen=strlen(buffer)+1;
		m.sender=malloc(m.senderLen*sizeof(char));
		strcpy(m.sender,buffer);

		buffer=sqlite3_column_text(prepared_sql, 1);
		m.textLen=strlen(buffer)+1;
		m.text=malloc(m.textLen*sizeof(char));
		strcpy(m.text,buffer);

		if(!(*callback)(&m, extra_parameters)){
			break;
		}
	}
	if(rc!=SQLITE_DONE && rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}

bool getUserSalt (
		const char * login,
		char salt[17]
	){
	
	if( !userExist(login)){
		return false;
	}


	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select salt "
			"from users "
			"where name = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare get salt [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, login, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind login\n");
		free(prepared_sql);
		return false;
	}

	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute\n");
		free(prepared_sql);
		return false;
	}

	strcpy(salt,sqlite3_column_text(prepared_sql, 0));
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "not single row\n");
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}

char * toB64(const unsigned char* data,int len){//len is real length no \0
	char b64[65] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-";
	int new_len=len+len/3+1+1;//1 for \0 and 1 for unclean cut 
	char * rep=malloc(new_len*sizeof(char));
	rep[0]='\0';
	int val=0;
	int step=0;
	int powers[]={1,4,16,64};
	for(int i=0;i<len-1;i++){
		val+=(((int)data[i])*powers[step])%64;
		strncat(rep,&b64[val],1);
		val=((int)data[i])/64;
		if(step==3){
			strncat(rep,&b64[val],1);
			val=0;
		}
		step=(step+1)%4;
	}
	if(val!=0){
		strncat(rep,&b64[val],1);
	}
	rep[new_len-1]='\0';
	return rep;
}


const char * hashIt(const char * pwd, const char * salt){
	unsigned char hash[64];
	unsigned char saltedPass[2048]="";
	int salt_len=strlen(salt);
	strcat(saltedPass, pwd );
	strcat(saltedPass, salt );
	while(strlen(saltedPass)+salt_len < 2048){
		strcat(saltedPass, salt );
	}
	SHA256(saltedPass, strlen(saltedPass), hash);
	const char * res=toB64(hash, 32);
	return res;
}

bool checkUserPassword (
		const char * login,
		const char * passwd
	){
	char salt[17];
	if(!getUserSalt(login, salt)){
		return false;
	}
	
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select name "
			"from users "
			"where name = ? and hash = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);


	if(rc){
		fprintf(stderr, "fail to prepare check pass [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}

	if(sqlite3_bind_text(prepared_sql, 1, login, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind login\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 2, hashIt(passwd, salt), -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind hash\n");
		free(prepared_sql);
		return false;
	}

	rc=sqlite3_step(prepared_sql);
	if(rc==SQLITE_DONE){
		return false;
	}
	if(rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	const char * forget = sqlite3_column_text(prepared_sql, 0);
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "not single row\n");
		return false;
	}
	
	sqlite3_finalize(prepared_sql);
	return true;
}

char *random_string(size_t size)
{
	char * str = malloc(size+1);
	const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-";
	if (size) {
		--size;
		for (size_t n = 0; n < size; n++) {
			int key = rand() % (int) (sizeof charset - 1);
			str[n] = charset[key];
		}
		str[size] = '\0';
	}
	return str;
}


bool createUser (
		const char * login,
		const char * passwd
	){
	if( userExist(login)){
		return false;
	}
	
	char * salt=random_string(16);
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"insert into users "
			"(name, hash, salt) "
			"values (?,?,?);",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare create user [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, login, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind login\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 2, hashIt(passwd, salt), -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind hash\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 3, salt, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind salt\n");
		free(prepared_sql);
		return false;
	}
	
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}

bool deleteUser (
		const char * login
	){
	if( !userExist(login)){
		fprintf(stderr, "user not found\n");
		return false;
	}

	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"delete from users "
			"where name=? "
			";",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare create user [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, login, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind login\n");
		free(prepared_sql);
		return false;
	}
		
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}


bool userExist (
		const char * login
	){
	
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select name "
			"from users "
			"where name = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);


	if(rc){
		fprintf(stderr, "fail to prepare check pass [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}

	if(sqlite3_bind_text(prepared_sql, 1, login, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind login\n");
		free(prepared_sql);
		return false;
	}

	rc=sqlite3_step(prepared_sql);
	if(rc==SQLITE_DONE){
		return false;
	}
	if(rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	const char * forget = sqlite3_column_text(prepared_sql, 0);
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "not single row\n");
		free(prepared_sql);
		return false;
	}
	
	sqlite3_finalize(prepared_sql);
	return true;
}
int getRoomMessageCount (
		const char * room_name
	){
	
	if( !roomExist(room_name)){
		return -1;
	}
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select count "
			"from rooms "
			"where name = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare get salt [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return -1;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, room_name, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind room_name\n");
		free(prepared_sql);
		return -1;
	}

	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute\n");
		free(prepared_sql);
		return -1;
	}
	int count = sqlite3_column_int(prepared_sql, 0);
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "not single row\n");
		free(prepared_sql);
		return -1;
	}
	sqlite3_finalize(prepared_sql);
	return count;
}


bool getRooms (
		const char * author,
		bool (*callback)(Room *, void *),
		void * extra_parameters,
		int maxCount
	){
	sqlite3_stmt * prepared_sql;
	int rc;
	if(strlen(author)==0){
		rc=sqlite3_prepare_v2(db,
				"select author, name "
				"from rooms "
				";",
				-1,
				&prepared_sql,
				NULL
			);
	}else{
		if(!userExist(author)){
			return false;
		}
		rc=sqlite3_prepare_v2(db,
				"select author, name "
				"from rooms "
				"where author = ? "
				";",
				-1,
				&prepared_sql,
				NULL
			);
	}
	if(rc){
		fprintf(stderr, "fail to prepare get rooms [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(strlen(author)!=0){
		if(sqlite3_bind_text(prepared_sql, 1, author, -1, SQLITE_STATIC)!=SQLITE_OK){
			fprintf(stderr, "fail to bind author\n");
			free(prepared_sql);
			return false;
		}
	}


	int i=-maxCount;
	while (true){
		if(i==0){
			break;
		}
		i++;
		rc=sqlite3_step(prepared_sql);
		if(rc!=SQLITE_ROW){
			break;
		}
		Room r;
		const char * buffer;
		buffer=sqlite3_column_text(prepared_sql, 0);
		strcpy(r.author,buffer);

		buffer=sqlite3_column_text(prepared_sql, 1);
		strcpy(r.name,buffer);

		if(!(*callback)(&r, extra_parameters)){
			break;
		}
	}
	if(rc!=SQLITE_DONE && rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}bool createRoom (
		const char * room_name,
		const char * author
	){
	if( !userExist(author)){
		fprintf(stderr, "user not found\n");
		return false;
	}
	if( roomExist(room_name)){
		fprintf(stderr, "room already exist\n");
		return false;
	}
	
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"insert into rooms "
			"(name, author, count) "
			"values (?,?,0);",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare create user [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, room_name, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind room_name\n");
		free(prepared_sql);
		return false;
	}
	if(sqlite3_bind_text(prepared_sql, 2, author, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind author\n");
		free(prepared_sql);
		return false;
	}
	
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}

bool deleteRoom (
		const char * room_name
	){
	if( !roomExist(room_name)){
		return false;
	}

	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"delete from rooms "
			"where name=? "
			";",
			-1,
			&prepared_sql,
			NULL
		);
	if(rc){
		fprintf(stderr, "fail to prepare create user [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	
	if(sqlite3_bind_text(prepared_sql, 1, room_name, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind room_name\n");
		free(prepared_sql);
		return false;
	}
		
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	sqlite3_finalize(prepared_sql);
	return true;
}

bool roomExist (
		const char * room_name
	){
	
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select name "
			"from rooms "
			"where name = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);


	if(rc){
		fprintf(stderr, "fail to prepare check pass [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}

	if(sqlite3_bind_text(prepared_sql, 1, room_name, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind room_name\n");
		free(prepared_sql);
		return false;
	}

	rc=sqlite3_step(prepared_sql);
	if(rc==SQLITE_DONE){
		return false;
	}
	if(rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	const char * forget = sqlite3_column_text(prepared_sql, 0);
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "not single row\n");
		free(prepared_sql);
		return false;
	}
	
	sqlite3_finalize(prepared_sql);
	return true;
}

bool userIsRoomOwner (
		const char * room_name,
		const char * author
	){
	
	sqlite3_stmt * prepared_sql;
	int rc=sqlite3_prepare_v2(db,
			"select name "
			"from rooms "
			"where name = ? and author = ? "
			";",
			-1,
			&prepared_sql,
			NULL
		);


	if(rc){
		fprintf(stderr, "fail to prepare check pass [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}

	if(sqlite3_bind_text(prepared_sql, 1, room_name, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind room_name\n");
		free(prepared_sql);
		return false;
	}

	if(sqlite3_bind_text(prepared_sql, 2, author, -1, SQLITE_STATIC)!=SQLITE_OK){
		fprintf(stderr, "fail to bind author\n");
		free(prepared_sql);
		return false;
	}

	rc=sqlite3_step(prepared_sql);
	if(rc==SQLITE_DONE){
		return false;
	}
	if(rc!=SQLITE_ROW){
		fprintf(stderr, "fail to execute [%s]\n", sqlite3_errmsg(db));
		free(prepared_sql);
		return false;
	}
	const char * forget = sqlite3_column_text(prepared_sql, 0);
	rc=sqlite3_step(prepared_sql);
	if(rc!=SQLITE_DONE){
		fprintf(stderr, "not single row\n");
		free(prepared_sql);
		return false;
	}
	
	sqlite3_finalize(prepared_sql);
	return true;
}

