## requirements
sudo apt install libsqlite3-dev

## usage
./bin/server <br/>
./bin/client [address] [pseudo] [password] [(l)ogin/(r)egister]

## structure
/ <br/>
├── bin //contient les exécutables <br/>
│   ├── client <br/>
│   ├── server <br/>
│   └── test //`make test` <br/>
├── Client_Serveur_V2 <br/>
│   ├── Client //code spécifique client <br/>
│   │   ├── client2.c <br/>
│   │   └── client2.h <br/>
│   ├── dataFrame.c //trame de données échangée <br/>
│   ├── dataFrame.h <br/>
│   ├── defined.h //constantes globales <br/>
│   └── Serveur //code spécifique serveur <br/>
│       ├── client2.h //structure `Client` et `Room` <br/>
│       ├── database.c //lien à la BDD <br/>
│       ├── database.h <br/>
│       ├── server2.c <br/>
│       └── server2.h <br/>
├── db.sqlite //persistance format sqlite3 <br/>
├── makefile <br/>
├── README.md <br/>
└── test.c //fichier de test de la BDD <br/>

## protocol

<table>
<tr>
    <td></td>
    <td>From</td>
    <td>To</td>
    <td>FrameData</td>
    <td>Content</td>
</tr>
<tr>
    <th colspan='5'>login</th>
</tr>
<tr>
    <td>unknown login</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"1234",isNew:false</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"wrong login or password",scope:4(deconnexion demandée)</td>
</tr>
<tr>
    <td>successful creation</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"1234",isNew:true</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"successful creation",scope:3(server 1-1 info)</td>
</tr>
<tr>
    <td>wrong passsword</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"123",isNew:false</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"wrong login or password",scope:4</td>
</tr>
<tr>
    <td>successful login</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"1234",isNew:false</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"welcome back",scope:3</td>
</tr>
<tr>
    <td>username is taken</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"pass",isNew:true</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"login is not available",scope:4</td>
</tr>
<tr>
    <td>multiple login</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"1234",isNew:false</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"welcome back",scope:3</td>
</tr>
<tr>
    <td>-</td>
    <td>Client2</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Flo",password:"1234",isNew:false</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client2</td>
    <td>Info</td>
    <td>text:"welcome back",scope:3</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"Flo logged in",scope:1(server to everyone)</td>
</tr>
<tr>
    <th colspan='5'>message</th>
</tr>
<tr>
    <td>send message</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Message</td>
    <td>sender:"Mario",text:"Hello World!"</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client2</td>
    <td>Message</td>
    <td>sender:"Mario",text:"Hello World!"</td>
</tr>
<tr>
    <td>message history</td>
    <td>Client3</td>
    <td>Serveur</td>
    <td>Login</td>
    <td>login:"Mario",password:"1234",isNew:true</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client3</td>
    <td>Info</td>
    <td>text:"successful creation",scope:3(server 1-1 info)</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client2</td>
    <td>Message</td>
    <td>sender:"Mario",text:"Hello World!"</td>
</tr>
<tr>
    <th colspan='5'>command</th>
</tr>
<tr>
    <td>successful room creation</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Command</td>
    <td>sender:"Mario",command:'+',roomName:"myRoom"</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"successful room creation",scope:3</td>
</tr>
<tr>
    <td>room name is taken</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Command</td>
    <td>sender:"Mario",command:'+',roomName:"myRoom"</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"room name not available",scope:3</td>
</tr>
<tr>
    <td>join a room</td>
    <td>Client1</td>
    <td>Serveur</td>
    <td>Command</td>
    <td>sender:"Mario",command:'>',roomName:"myRoom"</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"successfully joined",scope:3</td>
</tr>
<tr>
    <td>-</td>
    <td>Client2</td>
    <td>Serveur</td>
    <td>Command</td>
    <td>sender:"Mario",command:'>',roomName:"myRoom"</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client2</td>
    <td>Info</td>
    <td>text:"successfully joined",scope:3</td>
</tr>
<tr>
    <td>-</td>
    <td>Serveur</td>
    <td>Client1</td>
    <td>Info</td>
    <td>text:"<Client2> just slid into the room",scope:2(server to everyone in the room)</td>
</tr>


</table>
